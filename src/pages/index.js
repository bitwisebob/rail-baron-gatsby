import React from 'react'
import Link from 'gatsby-link'
import jquery from 'jquery'
import ColorPicker from '../components/colorpicker.js'
import LinePicker from '../components/linepicker.js'
import './map-style.css'
import MapSVG from '../components/map-svg.js'
import MapGrey from './map-grey.jpg'
import Helmet from 'react-helmet'

if (typeof window !== 'undefined'){
  window.$ = window.jQuery = jquery
}

let colors = 'None,Red,Blue,Green,Yellow,White,Black'.split(',');

let lines = [
  {id:"bm", name:"$4k Boston & Maine"},
  {id:"nynhh", name:"$4k New York, New Haven, & Hartford"},
  {id:"rfp", name:"$4k Richmond, Fredericksburg, & Potomac"},
  {id:"drgw", name:"$6k Denver & Rio Grande Western"},
  {id:"wp", name:"$8k Western Pacific"},
  {id:"tp", name:"$10k Texas & Pacific"},
  {id:"acl", name:"$12k Atlantic Coast Line"},
  {id:"gmo", name:"$12k Gulf, Mobile, & Ohio"},
  {id:"nw", name:"$12k Norfolk & Western"},
  {id:"cnw", name:"$14k Chicago & NorthWestern"},
  {id:"ic", name:"$14k Illinois Central"},
  {id:"np", name:"$14k Northern Pacific"},
  {id:"sal", name:"$14k Seaboard Airline"},
  {id:"gn", name:"$17k Great Northern"},
  {id:"cmstpp", name:"$18k Chicago, Milwauke, St. Paul, & Pacific"},
  {id:"ln", name:"$18k Louisville & Nashville"},
  {id:"slsf", name:"$19k St. Louis & San Francisco"},
  {id:"co", name:"$20k Chesapeake & Ohio"},
  {id:"cbq", name:"$20k Chicago, Burlington, & Quincy"},
  {id:"sou", name:"$20k Southern Railway"},
  {id:"mp", name:"$21k Missouri Pacific"},
  {id:"bo", name:"$24k Baltimore & Ohio"},
  {id:"nyc", name:"$28k New York Central"},
  {id:"crip", name:"$29k Chicago, Rock Island, and Pacific"},
  {id:"pa", name:"$30k Pennsylvania Railroad"},
  {id:"atsf", name:"$40k Atchison, Topeka, &Santa Fe"},
  {id:"up", name:"$40k Union Pacific"},
  {id:"sp", name:"$42k Southern Pacific"}
];

const MapPage = () => (
  <div id="mapWrapper">
    <Helmet>
      <script src="./functions.js"></script>
    </Helmet>
    <img src={MapGrey} />
    <MapSVG />
    <div id="controls">
      <button id="filter" type="button" className="button">Toggle Filter</button>
      <LinePicker lines={lines}/>
      <ColorPicker colors={colors}/>
      <button id="button" type="button" className="button">Apply Color</button>
    </div>

  <button type="button" className="button hidden" id="random">Random colors</button>
  </div>
)

export default MapPage
