import React from 'react'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'
import Helmet from 'react-helmet'

import './index.css'

const Header = () => (
  <div
    style={{
      background: '#333',
      padding: '0.25rem 1.0875rem',
      display: 'flex',
      display: 'none',
      justifyContent: 'flex-end',
      alignItems: 'middle',
    }}
  >
    <h1
      style={{
        color: '#fff',
        fontSize: '1rem',
        display: 'inline-block',
        margin: '0 auto 0 0',
      }}
    >
      Rail Baron
    </h1>
    <Link
      to="/"
      className="link"
      style={{
        color: 'white',
        textDecoration: 'none',
      }}
    >
      App
    </Link>
  </div>
)

const TemplateWrapper = ({ children }) => (
  <div>
    <Helmet
      title="Rail Baron App"
      meta={[
        { name: 'description', content: 'Rail Baron App' },
        { name: 'keywords', content: 'Rail Baron' },
      ]}
    />
    <Header />
    {children()}
  </div>
)

TemplateWrapper.propTypes = {
  children: PropTypes.func,
}

export default TemplateWrapper
