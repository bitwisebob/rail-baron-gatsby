module.exports = {
  pathPrefix: '/rail-baron-app',
  siteMetadata: {
    title: `Gatsby Default Starter`,
  },
  plugins: [`gatsby-plugin-react-helmet`],
}
